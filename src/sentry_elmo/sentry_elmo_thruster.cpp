/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_elmo/ElmoState.h"
#include "sentry_elmo/sentry_elmo_thruster.h"
#include "sentry_elmo_thruster_private.h"

#define ARTIFICIAL_DELAY 0.1
//#define INSERT_ARTIFICIAL_DELAY

namespace sentry_elmo
{
SentryElmoThruster::SentryElmoThruster()
  : ElmoMotorController(), d_ptr_(std::unique_ptr<SentryElmoThrusterPrivate>(new SentryElmoThrusterPrivate))
{
}
SentryElmoThruster::SentryElmoThruster(int argc, char** argv, const std::string& name)
  : ElmoMotorController(argc, argv, name)
  , d_ptr_(std::unique_ptr<SentryElmoThrusterPrivate>(new SentryElmoThrusterPrivate))
{
}

SentryElmoThruster::~SentryElmoThruster() = default;

void SentryElmoThruster::setMaxCommandedCurrent(double current)
{
  DS_D(SentryElmoThruster);
  d->max_commanded_current_ = fabs(current);
}
double SentryElmoThruster::maxCommandedCurrent() const noexcept
{
  const DS_D(SentryElmoThruster);
  return d->max_commanded_current_;
}
SentryElmoThruster::ThrusterPwm SentryElmoThruster::to_pwm(double pwm)
{
  if (pwm == 11 || pwm == 11.1)
  {
    return ThrusterPwm::PWM_11_1kHz;
  }
  else if (pwm == 22 || pwm == 22.2)
  {
    return ThrusterPwm::PWM_22_2kHz;
  }
  else if (pwm == 33 || pwm == 33.3)
  {
    return ThrusterPwm::PWM_33_3kHz;
  }
  else if (pwm == 44 || pwm == 44.4)
  {
    return ThrusterPwm::PWM_44_4kHz;
  }
  else
  {
    ROS_WARN_STREAM("Unknown PWM freq value: " << pwm);
    return ThrusterPwm::PWM_CUSTOM;
  }
}
void SentryElmoThruster::setName(const std::string& name)
{
  DS_D(SentryElmoThruster);
  d->name_ = name;
}

std::string SentryElmoThruster::name() const noexcept
{
  const DS_D(SentryElmoThruster);
  return d->name_;
}

bool SentryElmoThruster::abortEnableFlag() const noexcept
{
  const DS_D(SentryElmoThruster);
  return !(d->enable_state_ & EnableState::ABORT_FLAG_DISABLED);
}

void SentryElmoThruster::setAbortEnableFlag(const ds_core_msgs::Abort& abort)
{
  DS_D(SentryElmoThruster);
  if (!abort.enable)
  {
    d->enable_state_ |= EnableState::ABORT_FLAG_DISABLED;
  }
  else
  {
    d->enable_state_ &= ~(EnableState::ABORT_FLAG_DISABLED);
  }

  // if this is a disable command, go ahead and send a 0 current to make sure that happens
  if (!abort.enable)
  {
    setCommandedCurrent(d->current_off_);
  }
}
bool SentryElmoThruster::timedOut() const noexcept
{
  const DS_D(SentryElmoThruster);
  return d->enable_state_ & EnableState::TIMED_OUT;
}

bool SentryElmoThruster::motorEnabled() const noexcept
{
  const DS_D(SentryElmoThruster);
  return !(d->enable_state_ & EnableState::MOTOR_OUTPUT_DISABLED);
}

void SentryElmoThruster::setMotorEnabled(bool enabled)
{
  DS_D(SentryElmoThruster);
  if (!enabled)
  {
    d->enable_state_ |= EnableState::MOTOR_OUTPUT_DISABLED;
  }
  else
  {
    d->enable_state_ &= ~(EnableState::MOTOR_OUTPUT_DISABLED);
  }

  const auto command = std::string{ "MO=" + enabled ? "1\r" : "0\r" };

  auto io_command = ds_asio::IoCommand{ command, 0.2 };
#ifdef INSERT_ARTIFICIAL_DELAY
  io_command.setDelayAfter(ros::Duration(ARTIFICIAL_DELAY));
#endif
  io_command.setId(d->motor_output_cmd_id_);

  replaceRegularCommand(d->motor_output_cmd_id_, std::move(io_command));
}

SentryElmoThruster::EnableState SentryElmoThruster::outputEnabled() const noexcept
{
  const DS_D(SentryElmoThruster);
  return static_cast<EnableState>(d->enable_state_);
}

void SentryElmoThruster::setCommandedCurrent(const ds_actuator_msgs::ThrusterCmd& command)
{
  // Thruster names must match.
  if (command.thruster_name != name())
  {
    return;
  }

  DS_D(SentryElmoThruster);
  double ttl = command.ttl_seconds;
  if (ttl > d->max_ttl_)
  {
    ttl = d->max_ttl_;
  }

  d->cmdTimeout_ = ros::Time::now() + ros::Duration(ttl);
  if (ttl > 0)
  {
    // Clear timeout status
    d->enable_state_ &= ~(EnableState::TIMED_OUT);
  }
  setCommandedCurrent(command.cmd_value);
}

void SentryElmoThruster::setInvertCurrentEnabled(bool enabled)
{
  DS_D(SentryElmoThruster);
  d->inverted_current_ = enabled;
}

bool SentryElmoThruster::invertCurrentEnabled() const noexcept
{
  const DS_D(SentryElmoThruster);
  return d->inverted_current_;
}

void SentryElmoThruster::setCommandedCurrent(double current)
{
  DS_D(SentryElmoThruster);
  if (invertCurrentEnabled())
  {
    current = -current;
  }

  if (current > d->max_commanded_current_)
  {
    ROS_WARN_STREAM("Desired current greater than max: " << current << "(max: " << d->max_commanded_current_ << ")");
    current = d->max_commanded_current_;
  }
  else if (current < -d->max_commanded_current_)
  {
    ROS_WARN_STREAM("Desired current greater than max: " << current << "(max: " << -d->max_commanded_current_ << ")");
    current = -d->max_commanded_current_;
  }

  // if we're disabled, send 0
  if (d->enable_state_ != EnableState::ENABLED)
  {
    current = d->current_off_;
  }

  d->thruster_state_.cmd_value = static_cast<float>(current);
  const auto command = std::string{ "UF[1]=" + std::to_string(current) + "\r" };

  auto io_command = ds_asio::IoCommand{ command, 0.2 };
#ifdef INSERT_ARTIFICIAL_DELAY
  io_command.setDelayAfter(ros::Duration(ARTIFICIAL_DELAY));
#endif
  io_command.setId(d->current_cmd_id_);

  replaceRegularCommand(d->current_cmd_id_, std::move(io_command));
}

void SentryElmoThruster::setupParameters()
{
  ElmoMotorController::setupParameters();

  DS_D(SentryElmoThruster);
  d->pwm_ = to_pwm(ros::param::param<double>("~pwm", 11));
  d->command_topic_ = ros::param::param<std::string>("~command_topic", "thruster_command");
  d->abort_topic_ = ros::param::param<std::string>("~abort_topic", "abort");
  d->max_ttl_ = ros::param::param<double>("~max_thruster_ttl", 60);
  d->current_off_ = ros::param::param<double>("~off_current", 0.0);
  setCommsTimeout(ros::Duration(ros::param::param<double>("~comms_timeout", 1)));
  setName(ros::param::param<std::string>("~name", "thruster"));
  setInvertCurrentEnabled(ros::param::param<bool>("~invert_current", false));

  const auto dynamics_ns = ros::param::param<std::string>("dynamics_ns", "");
  auto dynamics_param = ros::names::resolve(dynamics_ns, { "thruster_current_limit" });
  setMaxCommandedCurrent(ros::param::param<double>(dynamics_param, 10.0));
}

void SentryElmoThruster::setup()
{
  ElmoMotorController::setup();

  DS_D(SentryElmoThruster);

  // Find the current setting command
  const auto cmds = polledCommands();

  const auto findCommand = [&cmds](const std::string& cmd) {
    return std::find_if(std::begin(cmds), std::end(cmds), [&cmd](const ElmoMotorController::PollCommand& p) -> bool {
      return p.second.getCommand().find(cmd) != std::string::npos;
    });
  };

  const auto uf_it = findCommand("UF[1]=");

  if (uf_it == std::end(cmds))
  {
    ROS_FATAL_STREAM("Unable to find 'UF[1]=' command in our polling queue.");
    ROS_BREAK();
  }

  d->current_cmd_id_ = uf_it->first;

  const auto mo_it = findCommand("MO=");
  if (uf_it == std::end(cmds))
  {
    ROS_FATAL_STREAM("Unable to find 'MO=' command in our polling queue.");
    ROS_BREAK();
  }
  d->motor_output_cmd_id_ = mo_it->first;
}

void SentryElmoThruster::setupSubscriptions()
{
  DsProcess::setupSubscriptions();

  DS_D(SentryElmoThruster);

  // ROS (well, BOOST in this case) can't handle member function overloads in bind, so we need to
  // select the right one ourself.  Or we could have just had two differently named member functions...
  // but that would be too easy.
  //
  // The syntax for the cast reads:
  //   static_cast<return_type(ClassName::*)(method_signature)>(method_address)
  //
  auto member_overload = static_cast<void (SentryElmoThruster::*)(const ds_actuator_msgs::ThrusterCmd&)>(
      &SentryElmoThruster::setCommandedCurrent);

  d->command_sub_ = nodeHandle().subscribe(d->command_topic_, 10, member_overload, this);

  d->abort_sub_ = nodeHandle().subscribe(d->abort_topic_, 10, &SentryElmoThruster::setAbortEnableFlag, this);
}

void SentryElmoThruster::setupPublishers()
{
  DsProcess::setupPublishers();
  DS_D(SentryElmoThruster);
  auto nh = nodeHandle();
  d->elmo_state_pub_ = nh.advertise<ElmoState>(ros::this_node::getName() + "/elmo_state", 10, false);
  d->thruster_state_pub_ =
      nh.advertise<ds_actuator_msgs::ThrusterState>(ros::this_node::getName() + "/state", 10, false);
}

void SentryElmoThruster::setupTimers()
{
  DsProcess::setupTimers();
  DS_D(SentryElmoThruster);

  const auto period = ros::param::param<float>("~state_update_period", 0.1);
  d->elmo_state_timer_ = nodeHandle().createTimer(
      ros::Duration(period), boost::bind(&SentryElmoThrusterPrivate::sendElmoStatusMessage, d, this, _1));
}

void SentryElmoThruster::setupCommands()
{
  DS_D(SentryElmoThruster);

  auto command = ds_asio::IoCommand{ "UM=1\r", 0.2, false };
#ifdef INSERT_ARTIFICIAL_DELAY
  command.setDelayAfter(ros::Duration(ARTIFICIAL_DELAY));
#endif
  addPreemptCommand(std::move(command));

  // Add pwm setup parameters to the init list
  for (auto& cmd : d->init_pwm_commands(d->pwm_))
  {
    command = ds_asio::IoCommand{ cmd, 0.2, false };
#ifdef INSERT_ARTIFICIAL_DELAY
    command.setDelayAfter(ros::Duration(ARTIFICIAL_DELAY));
#endif
    addPreemptCommand(std::move(command));
  }

  const auto init_commands = std::list<std::string>{ { "CL[2]=1\r" }, { "CL[1]=10.5\r" }, { "PL[1]=10.5\r" },
                                                     { "PL[2]=2\r" }, { "MO=1\r" },       { "TC=0.0\r" } };

  for (auto& cmd : init_commands)
  {
    command = ds_asio::IoCommand{ cmd, 0.2, false };
#ifdef INSERT_ARTIFICIAL_DELAY
    command.setDelayAfter(ros::Duration(ARTIFICIAL_DELAY));
#endif
    addPreemptCommand(command);
  }

  ElmoMotorController::setupCommands();

  const auto polling_commands =
      std::list<std::string>{ { "MO=1\r" }, { "MO\r" }, { "UF[1]=0.0\r" }, { "XQ##DM\r" }, { "TC\r" },
                              { "SR\r" },   { "MF\r" }, { "UM\r" },        { "PX\r" },     { "VX\r" },
                              { "IQ\r" },   { "ID\r" }, { "AN[6]\r" },     { "TI[1]\r" },  { "UF[1]\r" } };

  for (auto cmd : polling_commands)
  {
    const auto io_command = ds_asio::IoCommand{ cmd, 0.2, false };
#ifdef INSERT_ARTIFICIAL_DELAY
    command.setDelayAfter(ros::Duration(ARTIFICIAL_DELAY));
#endif
    addRegularCommand(std::move(io_command));
  }
}

void SentryElmoThruster::checkProcessStatus(const ros::TimerEvent& event)
{
  auto status = statusMessage();
  const auto now = ros::Time::now();

  DS_D(SentryElmoThruster);

  // Perform our timeout-check for command TTLs
  if (ros::Time::now() > d->cmdTimeout_)
  {
    d->enable_state_ |= EnableState::TIMED_OUT;
    setCommandedCurrent(d->current_off_);
  }

  auto mf = parameter("MF");
  if (!mf.first.isValid())
  {
    ROS_ERROR("No recorded 'failure' value (MF)!  No comms with thruster?");
    status.status = ds_core_msgs::Status::STATUS_ERROR;
    publishStatus(status);
    return;
  }

  auto failure = static_cast<uint64_t>(mf.second);

  if (commsTimeout() > ros::Duration(0) && (now - mf.first) > commsTimeout())
  {
    ROS_ERROR("'MF' value too old (%.2f seconds) !  No comms with thruster?", (now - mf.first).toSec());
    status.status = ds_core_msgs::Status::STATUS_ERROR;
  }
  else if (failure != 0)
  {
    ROS_WARN("'MF' indicating fault condition: 0x%lx", failure);
    status.status = ds_core_msgs::Status::STATUS_WARN;
  }
  else if (d->enable_state_ != EnableState::ENABLED)
  {
    ROS_WARN("Thruster driver node %s is disabled!  Status: 0x%x (motor: %d, abort: %d, timeout: %d)",
             ros::this_node::getName().data(), outputEnabled(), motorEnabled(), abortEnableFlag(), timedOut());
  }

  publishStatus(status);
}

void SentryElmoThruster::setCommsTimeout(ros::Duration timeout)
{
  DS_D(SentryElmoThruster);
  d->elmo_comms_timeout_ = std::move(timeout);
}
ros::Duration SentryElmoThruster::commsTimeout() const noexcept
{
  const DS_D(SentryElmoThruster);
  return d->elmo_comms_timeout_;
}

bool SentryElmoThruster::setMotorEnabled(std_srvs::SetBoolRequest& req, std_srvs::SetBoolResponse& res)
{
  setMotorEnabled(req.data);
  res.success = true;
  return true;
}

void SentryElmoThruster::setupServices()
{
  DsProcess::setupServices();

  DS_D(SentryElmoThruster);
  d->motor_enable_srv_ = nodeHandle().advertiseService<std_srvs::SetBool::Request, std_srvs::SetBool::Response>(
      ros::this_node::getName() + "/motor_enable", boost::bind(&SentryElmoThruster::setMotorEnabled, this, _1, _2));
}
}
