/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_elmo_thruster_private.h"
#include "sentry_elmo/ElmoState.h"

namespace sentry_elmo
{
SentryElmoThrusterPrivate::SentryElmoThrusterPrivate()
  : pwm_(SentryElmoThruster::ThrusterPwm::PWM_11_1kHz)
  , max_commanded_current_(10.0)
  , inverted_current_(false)
  , current_off_(0.0)
{
}

SentryElmoThrusterPrivate::~SentryElmoThrusterPrivate() = default;

std::list<std::string> SentryElmoThrusterPrivate::init_pwm_commands(SentryElmoThruster::ThrusterPwm pwm_mode)
{
  switch (pwm_mode)
  {
    case SentryElmoThruster::ThrusterPwm::PWM_11_1kHz:
    {
      ROS_INFO_STREAM("Initializing PWM to 11.1kHz");
      return { "XP[2]=0\r", "XP[6]=22563\r", "KP[1]=2.937\r", "KI[1]=3176\r" };
    }
    case SentryElmoThruster::ThrusterPwm::PWM_22_2kHz:
    {
      ROS_INFO_STREAM("Initializing PWM to 22.2kHz");
      return { "XP[2]=2\r", "XP[6]=23655\r", "KP[1]=2.209\r", "KI[1]=2946\r" };
    }
    case SentryElmoThruster::ThrusterPwm::PWM_33_3kHz:
    {
      ROS_INFO_STREAM("Initializing PWM to 33.3kHz");
      return { "XP[2]=3\r", "XP[6]=24835\r", "KP[1]=2.36\r", "KI[1]=3092\r" };
    }
    case SentryElmoThruster::ThrusterPwm::PWM_44_4kHz:
    {
      ROS_INFO_STREAM("Initializing PWM to 44.4kHz");
      return { "XP[2]=4\r", "XP[6]=24518\r", "KP[1]=2.322\r", "KI[1]=3186\r" };
    }
    case SentryElmoThruster::ThrusterPwm::PWM_CUSTOM:
    {
      ROS_INFO_STREAM("Custom PWM initialization from config file.");
      return {};
    }
  }
}

void SentryElmoThrusterPrivate::sendElmoStatusMessage(SentryElmoThruster* base, const ros::TimerEvent&)
{
  elmo_state_.header.stamp = ros::Time::now();
  elmo_state_.ds_header.io_time = elmo_state_.header.stamp;
  const auto uuid = base->uuid();
  std::copy(std::begin(uuid.data), std::end(uuid.data), std::begin(elmo_state_.ds_header.source_uuid));

  thruster_state_.header = elmo_state_.header;
  thruster_state_.ds_header = elmo_state_.ds_header;
  thruster_state_.actual_value = 0;

  auto parameters = base->parameters();

  try
  {
    elmo_state_.current_reactive = parameters.at("ID").second;
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.current_active = parameters.at("IQ").second;
    thruster_state_.actual_value = static_cast<float>(elmo_state_.current_active);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.torque = parameters.at("TC").second;
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.line_voltage = parameters.at("AN[6]").second;
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.main_position = static_cast<int>(parameters.at("PX").second);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.main_feedback_vel = static_cast<int>(parameters.at("VX").second);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.status = static_cast<uint32_t>(parameters.at("SR").second);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.enabled = static_cast<unsigned char>(parameters.at("MO").second == 1);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.mode = static_cast<uint8_t>(parameters.at("UM").second);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.failure = static_cast<uint32_t>(parameters.at("MF").second);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.absolute_position = static_cast<int>(parameters.at("PA").second);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.relative_position = static_cast<int>(parameters.at("PR").second);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.jog_velocity = static_cast<int>(parameters.at("JV").second);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.temperature = static_cast<int>(parameters.at("TI[1]").second);
  }
  catch (std::out_of_range&)
  {
  }
  try
  {
    elmo_state_.pwm_speed = static_cast<uint64_t>(parameters.at("XP[2]").second);
  }
  catch (std::out_of_range&)
  {
  }

  elmo_state_pub_.publish(elmo_state_);
  thruster_state_pub_.publish(thruster_state_);
}
}
