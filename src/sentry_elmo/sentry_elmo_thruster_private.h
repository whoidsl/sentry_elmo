/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_ELMO_SENTRYTHRUSTER_PRIVATE_H
#define SENTRY_ELMO_SENTRYTHRUSTER_PRIVATE_H

#include "sentry_elmo/sentry_elmo_thruster.h"
#include "ds_actuator_msgs/ThrusterState.h"
#include "sentry_elmo/ElmoState.h"

namespace sentry_elmo
{
struct SentryElmoThrusterPrivate
{
  SentryElmoThrusterPrivate();
  ~SentryElmoThrusterPrivate();

  /// @brief Return a list of elmo commands to initialize the desired PWM freq
  ///
  /// \param pwm_mode
  /// \return
  std::list<std::string> init_pwm_commands(SentryElmoThruster::ThrusterPwm pwm_mode);

  void sendElmoStatusMessage(SentryElmoThruster* base, const ros::TimerEvent&);

  SentryElmoThruster::ThrusterPwm pwm_;
  std::string name_;
  uint64_t current_cmd_id_;       //!< State machine ID for the current command
  uint64_t motor_output_cmd_id_;  //!< State machine ID for the motor output command
  double max_commanded_current_;
  unsigned int enable_state_ = SentryElmoThruster::EnableState::ENABLED;
  ros::Time cmdTimeout_;
  double max_ttl_;
  double current_off_;  //!< Current sent when the thruster should be "off"

  std::string command_topic_;
  std::string abort_topic_;

  ros::Subscriber command_sub_;
  ros::Subscriber abort_sub_;
  ros::Publisher elmo_state_pub_;
  ros::Publisher thruster_state_pub_;
  ros::Timer elmo_state_timer_;
  ros::Duration elmo_comms_timeout_;
  ros::ServiceServer motor_enable_srv_;
  bool inverted_current_;
  ds_actuator_msgs::ThrusterState thruster_state_;
  ElmoState elmo_state_;
};
}
#endif  // SENTRY_ELMO_SENTRYTHRUSTER_PRIVATE_H
