/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_ELMO_SENTRYTHRUSTER_H
#define SENTRY_ELMO_SENTRYTHRUSTER_H

#include <ds_elmo/elmomotorcontroller.h>
#include <ds_actuator_msgs/ThrusterCmd.h>
#include <ds_core_msgs/Abort.h>

#include "std_srvs/SetBool.h"
namespace sentry_elmo
{
struct SentryElmoThrusterPrivate;

///@brief Elmo controller setup for Sentry AUV
///
/// # Parameters
///
/// In addition to the parameters used by `DsProcess`, `ElmoMotorController` looks for the
/// following:
///
///   - `~init_commands`:  A list of commands to send to the controller at startup
///   - `~poll_commands`:  A list of commands to regularly poll the controller with
///   - `~pwm`:            The thruster PWM frequency, see section below.
///   - `~command_topic`:  Topic to listen for commands on.
///   - `~comms_timeout`:  Maximum age for comms before error flags are raised.
///   - `~name`:           The thruster name (must match `ds_actuator_msgs::ThrusterCmd.thruster_name`)
///   - `~invert_current`: Invert the current sign (for reverse-pitch props)
///
/// # "Dynamic Parameters"
///
/// Many of Sentry's parameters live in /sentry/dynamics and are shared by many nodes.  To support this,
/// provide a *non-private* parameter `dynamics_ns`.  For example:
///
///  dynamics_ns: /sentry/dynaimcs
///
/// The following parameters in this namespace are looked for:
///
///   - `thruster_current_limit` Set the maximum commanded torque current.
///
/// # PWM Modes
///
/// Sentry runs the thrusters in "Amplitude Mode" at a specified PWM.  The following
/// values are valid:
///
/// - 11 (or 11.1) for 11.1kHz (default)
/// - 22 (or 22.2) for 22.2kHz
/// - 33 (or 33.3) for 33.3kHz
/// - 44 (or 44.4) for 44.4kHz
///
/// The class will insert the correct initialization commands depending on the value given.
class SentryElmoThruster : public ds_elmo::ElmoMotorController
{
  DS_DECLARE_PRIVATE(SentryElmoThruster)

public:
  SentryElmoThruster();
  SentryElmoThruster(int argc, char* argv[], const std::string& name);
  ~SentryElmoThruster() override;
  DS_DISABLE_COPY(SentryElmoThruster)

  enum class ThrusterPwm
  {
    PWM_11_1kHz = 0,   //!< Use 11.1kHz PWM
    PWM_22_2kHz,       //!< Use 22.2kHz PWM
    PWM_33_3kHz,       //!< Use 33.3kHz PWM
    PWM_44_4kHz,       //!< Use 44.4kHz PWM
    PWM_CUSTOM = 255,  //!< Initialize PWM from config file.
  };

  /// Elmo thruster enable state
  ///
  /// The thruster can be DISabled multiple ways:
  ///
  /// - By disabling the motor output
  /// - By receiving an abort message with the 'enable' flag set false
  /// - By receiving a command with a TTL that has expired.
  enum EnableState
  {
    ENABLED = 0x00,                //!< Output is enabled
    MOTOR_OUTPUT_DISABLED = 0x01,  //!< Motor output (MO=?) is DISABLED
    ABORT_FLAG_DISABLED = 0x02,    //!< Abort status has disabled the thruster
    TIMED_OUT = 0x04,              //!< TTL timed out
  };

  static ThrusterPwm to_pwm(double pwm);

  /// @brief Set the thruster name
  ///
  /// The thruster name is used to match against 'thruster_name' field in
  /// ds_actuator_msgs::ThrusterCommand.  If the two strings match the command
  /// will be run
  ///
  /// \param name
  void setName(const std::string& name);

  /// @breif Get the thruster name
  ///
  /// The thruster name is used to match against 'thruster_name' field in
  /// ds_actuator_msgs::ThrusterCommand.  If the two strings match the command
  /// will be run
  ///
  /// \return
  std::string name() const noexcept;

  /// @brief Set the maximum commanded current limit
  ///
  /// Required currents calculated for the provided thrust values will
  /// 'saturate' at this value (in both directions).  This affects outgoing
  /// current commands to the elmo motor controller and is independant of the
  /// controller's own current limit parameters.
  ///
  /// \param current   (amps)
  void setMaxCommandedCurrent(double current);

  /// @brief Get the maximum commanded current limit
  ///
  /// Required currents calculated for the provided thrust values will
  /// 'saturate' at this value (in both directions).  This affects outgoing
  /// current commands to the elmo motor controller and is independant of the
  /// controller's own current limit parameters.
  ///
  /// \return
  double maxCommandedCurrent() const noexcept;

  /// Set the commanded current
  ///
  /// \param current
  void setCommandedCurrent(double current);

  /// Set the commanded current from a thruster command message.
  ///
  /// NOTE:  The `thruster_name` field in the command must *exactly* match
  /// the name of the thruster set using `setName` for the command to be
  /// acted upon.
  ///
  /// \param command
  void setCommandedCurrent(const ds_actuator_msgs::ThrusterCmd& command);

  /// Get the thruster enable flag from the last abort message
  ///
  /// \return
  bool abortEnableFlag() const noexcept;

  /// Set the thruster enable flag from an abort message
  /// \param abort An abort message that can set whether the thruster is enabled or not
  void setAbortEnableFlag(const ds_core_msgs::Abort& abort);

  /// Return whether the motor output enabled?
  ///
  /// Returns the state of the "MO" elmo parameter
  /// \return
  bool motorEnabled() const noexcept;

  /// Enable or diable motor output
  ///
  /// Sets the "MO" elmo parameter.  When the motor is disabled the drive stage of the controller
  /// is shut down.
  ///
  /// \param enabled
  void setMotorEnabled(bool enabled);

  /// Set when a TTL has expired
  ///
  /// \return
  bool timedOut() const noexcept;

  /// Return the enable state of the node
  ///
  /// Motor output can be disabled a number of ways:
  ///
  /// - By disabling the motor output
  /// - By receiving an abort message with the 'enable' flag set false
  /// - By receiving a command with a TTL that has expired.
  ///
  /// This method returns the current raw state value.  You can check individual states using
  /// the `motorEnabled()`, `abortFlagEnabled()` and `timedOut()` methods
  /// \return
  EnableState outputEnabled() const noexcept;

  /// @brief Set the expected maximum age of commands.
  ///
  /// This timeout controls the age at which parameters are considered
  /// "too old" for the health status check, perhaps indicating loss of comms
  /// with the elmo driver.
  ///
  ///
  /// Values less than 0 disable this check.
  ///
  /// \param timeout
  void setCommsTimeout(ros::Duration timeout);

  /// @brief Get the expected maximum age of commands
  ///
  /// See SentryThruster::setCommsTimeout
  /// \return
  ros::Duration commsTimeout() const noexcept;

  /// @brief Invert the sign of the current in commands.
  ///
  /// Reverses the direction of the motor controller for the provided
  /// current.  Useful when using reverse-pitched props.
  void setInvertCurrentEnabled(bool enabled);

  /// @brief Inverted current commands enabled.
  ///
  /// \return
  bool invertCurrentEnabled() const noexcept;

  void setup() override;

  bool setMotorEnabled(std_srvs::SetBoolRequest& req, std_srvs::SetBoolResponse& res);

protected:
  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;
  void setupTimers() override;
  void setupCommands() override;
  void setupServices() override;

  void checkProcessStatus(const ros::TimerEvent& event) override;

private:
  std::unique_ptr<SentryElmoThrusterPrivate> d_ptr_;
};
}
#endif  // DS_ELMO_SENTRYTHRUSTER_H
